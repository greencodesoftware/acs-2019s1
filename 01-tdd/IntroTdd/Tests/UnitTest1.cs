using System;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        private Speed Speed { get; }

        public UnitTest1()
        {
            Speed = new Speed(10, new KmPerHour());
        }

        [Fact]
        public void Test001_ASpeedValueWhenIncreasePercentShouldUpdateValue()
        {
            //setup Testee
            //execute (estimulo)
            Speed.IncreasePercent(20);
            //assertions (verificaciones)
            Assert.Equal(12, Speed.Value);
            //tear down environment
        }

        [Fact]
        public void Test002_ASpeedValueWhenIncreaseAbsolutShouldUpdateValue()
        {
            Speed.IncreaseAbsolut(20);
            Assert.Equal(30, Speed.Value);
        }

        [Fact]
        public void Test003_SpeedValueWhenDecreasedBelowZeroShouldFailAndNotUpdate()
        {
            Assert.Throws<InvalidOperationException>(() => Speed.IncreaseAbsolut(-20));
            Assert.Equal(10, Speed.Value);
        }
    }

    public class Speed
    {
        public double Value { get; private set; }

        public Speed(double value, KmPerHour kmPerHour)
        {
            Value = value;
        }

        public void IncreasePercent(double percentage)
        {
            Value *= percentage / 100 + 1;
        }

        public void IncreaseAbsolut(double valueToIncrease)
        {
            if (Value + valueToIncrease < 0)
                throw new InvalidOperationException();
            Value += valueToIncrease;
        }
    }

    public class KmPerHour
    {
    }
}
