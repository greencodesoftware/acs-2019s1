
npm install cypress --save-dev

npx cypress open
    o bien en package.json file agregar la config y luego correr npm run cypress:open
        {
            "scripts": {
                "cypress:open": "cypress open"
            }
        }

correr y analizar samples

que es entonces cypress? o selenium? que tradeoffs tenemos?
    Pro's
        browser embedded
        Shortcuts "You no longer have to use your UI to build up state"
        Flake resistant "same app loop, sync"
        Debuggability
    Permanent trade-offs:
        Cypress is not a general purpose automation tool => e2e ui functional testing
        Cypress commands run inside of a browser => it makes it a little bit harder to communicate with the backend 
        There will never be support for multiple browser tabs => por target blank hace otro test para esa url
        You cannot use Cypress to drive two browsers at the same time
        Each test is bound to a single origin => limited to only visiting a single superdomain, de primer orden

xunit vs specs o TDD vs BDD

walkthrough spec fwks APIs

provoquemos un error

cy.pause()

cy.debug() y uso de devtools en chrome y analisis de devtools
    cy.get('.assertion-table').debug()

cypress.json
    {
        "baseUrl": "http://localhost:8080"
    }
    entonces los visit pueden ser: cy.visit('/')

Stubbing the server => https://docs.cypress.io/guides/guides/network-requests.html#Routing
    cy.server()           // enable response stubbing
    cy.route({
    method: 'GET',      // Route all GET requests
    url: '/users/*',    // that have a URL that matches '/users/*'
    response: []        // and force the response to be: []
    })
    When you start a cy.server() and define cy.route() commands, Cypress displays this under “Routes” in the Command Log.

cy.request('/test_helpers/seed_users')
    Request is not displayed in the Network Tab of Developer Tools, it is performed from the test runner process

cy.readFile() para hacer "dinamico un test"

cy.task()
    arbitrary Node code outside of the scope of Cypress
        Seeding your test database.
        Storing state in Node that you want persisted between spec files.
        Performing parallel tasks, like making multiple http requests outside of Cypress.
        Running an external process.

cy.exec() idem anterior pero ejecuta por consola en vez de nodejs

Analizar caso de Login => https://docs.cypress.io/guides/getting-started/testing-your-app.html#Logging-in

Integracion continua
    npm start & wait-on http://localhost:8080 & cypress run
    Recording tests allow you to => https://www.cypress.io/dashboard/
        See the number of failed, pending and passing tests.
        Get the entire stack trace of failed tests.
        View screenshots taken when tests fail and when using cy.screenshot().
        Watch a video of your entire test run or a clip at the point of test failure.
        See which machines ran each test when parallelized.
        cmd => cypress run --record --key=abc123 --parallel
