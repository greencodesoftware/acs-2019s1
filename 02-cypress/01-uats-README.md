Extreme Programming: projects / us / uat
    http://www.extremeprogramming.org/map/project.html
    http://www.extremeprogramming.org/rules/userstories.html
        http://www.jpattonassociates.com/wp-content/uploads/2015/03/story_essentials_quickref.pdf
    http://www.extremeprogramming.org/rules/functionaltests.html

=================================================================

Ejemplo UAT agile:

me dirijo al tablero de tareas:
  visitar: sitio.com
  Log in como:
      user
      123
  abrir tablero de tareas

me dirijo al tablero de tareas
marcar como archivada la primer tarea
resultado esperado:
    la tarjeta deja de estar en el tablero
    dirigirse al archivo de tareas y la primer tarea debe ser la archivada


me dirijo al tablero de tareas
marcar como archivada la primer tarea
resultado esperado:
    la tarjeta deja de estar en el tablero
    dirigirse al archivo de tareas y la primer tarea debe ser la archivada


me dirijo al tablero de tareas
marcar como archivada la primer tarea
resultado esperado:
    la tarjeta deja de estar en el tablero
    dirigirse al archivo de tareas y la primer tarea debe ser la archivada

me dirijo al tablero de tareas
marcar como archivada la primer tarea
resultado esperado:
    la tarjeta deja de estar en el tablero
    dirigirse al archivo de tareas y la primer tarea debe ser la archivada



=================================================================

Agreguemos USM!
    https://www.jpattonassociates.com/user-story-mapping/
    http://www.jpattonassociates.com/wp-content/uploads/2015/03/story_mapping.pdf

=================================================================

USER STORIES MAP (6 MESES DE VISION)
   ;;;;;
   ;;;;;
 ..;;;;;..
  ':::::'
    ':`
USER STORIES DEV BACKLOG & RELEASE PLANNING (2 MES DE VISION, ESTIMACION SP FIBONACCI)
   ;;;;;
   ;;;;;
 ..;;;;;..
  ':::::'
    ':`
USER ACCEPTANCE TESTS (1 O 2 SPRINTS DE VISION)
   ;;;;;
   ;;;;;
 ..;;;;;..
  ':::::'
    ':`
DEVTEAM TASK BREAKOUT (SPRINT EN CURSO)
   ;;;;;
   ;;;;;
 ..;;;;;..
  ':::::'
    ':`
DEV TDD / BDD (TAREA EN CURSO)
   ;;;;;
   ;;;;;
 ..;;;;;..
  ':::::'
    ':`
CI PIPELINING (POR COMMIT, NOCTURNOS, ON DEMAND)
