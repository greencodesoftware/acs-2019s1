0) Concepto de Regression testing

1) Docker

Intro, que es un container?
	container vs vm => host virtualization vs kernel virtualization
	https://docs.docker.com/get-started/overview/
	https://www.docker.com/resources/what-container

Use Cases
	https://success.docker.com/article/dev-pipeline
	https://goto.docker.com/rs/929-FJL-178/images/20150731-wp_docker-3-ways-devops.pdf

Orchestrators
	https://docs.docker.com/ee/docker-ee-architecture/
	Immutable Infra Stack
		docker
		image registry
		docker-compose
		docker-machine
		docker-toolbox & boot2docker
		kitematic
		cluster managers
			docker-swarm
			mesos
			kubernetes
			openshift
			rancher

2) CI

Jenkins
	que es?
		https://jenkins.io/doc/pipeline/tour/hello-world/
	ver listado =>
		http://ci.greencodesoftware.com/jenkins/view/ladevi/
	ver configure
	ver workspace
	output build history =>
		http://ci.greencodesoftware.com/jenkins/view/ladevi/builds
	ver console outputs

circle-ci
	https://bitbucket.org/asmx1986/curso-angular6/commits/all
		travis => https://circleci.com/bb/asmx1986/curso-angular6/14?utm_campaign=vcs-integration-link&utm_medium=referral&utm_source=bitbucket-build-link
		config docker & tdd & cypress => https://circleci.com/bb/asmx1986/curso-angular6/14?utm_campaign=vcs-integration-link&utm_medium=referral&utm_source=bitbucket-build-link

TP Challenge => que levante docker de una web app front y otro docker para el back y correr unit tests y UI tests

3) CD

spinnaker
https://www.spinnaker.io/
manual judgement
	https://www.spinnaker.io/guides/tutorials/codelabs/safe-deployments/#changing-pipeline-behavior-based-on-selected-judgment

TP Challenge:
	deploy a VM cloud o heroku
	https://docs.travis-ci.com/user/deployment/heroku/


4) Instalar docker y docker compose, y levantar api y locust
